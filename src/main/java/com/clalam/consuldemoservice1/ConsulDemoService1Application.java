package com.clalam.consuldemoservice1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient // Not required anymore
public class ConsulDemoService1Application {

	public static void main(String[] args) {
		SpringApplication.run(ConsulDemoService1Application.class, args);
	}

}
