package com.clalam.consuldemoservice1.domain;

public record Student(String name, String clazz) {

  public Student() {
    this(null, null);
  }
}